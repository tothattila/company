/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company;

import company.repository.impl.CSVEmployeeRepository;
import company.view.DisplayService;

/**
 *
 * @author Toth.Attila
 */
public class Application {
    public static void main(String[] args) {
        DisplayService displayService = new DisplayService(new CSVEmployeeRepository());
        displayService.showMenu();
        displayService.action('1');
    }
}
