/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company.repository;

import company.model.Employee;
import java.util.List;

/**
 *
 * @author Toth.Attila
 */
public interface EmployeeRepository {
    public void save(Employee e);
    
    public List<Employee> findall();
    
    public Employee findByID(int id);
    
    
}
