/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company.repository.impl;

import company.model.Employee;
import company.repository.EmployeeRepository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Toth.Attila
 */
public class CacheEmployeeRepostitory implements EmployeeRepository {
    List<Employee> employees = new ArrayList<>();

    @Override
    public void save(Employee e) {
        employees.add(e);
    }

    @Override
    public List<Employee> findall() {
        return employees;
    }

    @Override
    public Employee findByID(int id) {
        return null;
    }
    
}
