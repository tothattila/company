/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company.view;

import company.repository.EmployeeRepository;

/**
 *
 * @author Toth.Attila
 */
public class DisplayService {
    EmployeeRepository emplyEmployeeRepository;

    public DisplayService(EmployeeRepository emplyEmployeeRepository) {
        this.emplyEmployeeRepository = emplyEmployeeRepository;
    }
    
    public void showMenu(){
    
    }
    
    public void action(char c){
        switch(c){
            case '1':
                emplyEmployeeRepository.findall().forEach(System.out::println);
                break;
        }
    }
    
}
